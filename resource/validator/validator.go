package validator

import "gitlab.com/naufalfmm/url-shortener/utils/validator"

func NewValidator() (validator.Validator, error) {
	return validator.NewV9()
}
