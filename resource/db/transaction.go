package db

import "github.com/go-redis/redis/v8"

type Transaction struct {
	trx    *redis.Pipeliner
	isUsed bool
}

func (trx *Transaction) Ignore() {
	trx.isUsed = false
}

func (trx *Transaction) Acknowledge() {
	trx.isUsed = true
}
