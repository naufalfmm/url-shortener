package db

import (
	"context"

	"github.com/go-redis/redis/v8"
)

type DB struct {
	*redis.Client
	transaction *Transaction
}

func (db *DB) StartTransaction(ctx context.Context) {
	if db.transaction == nil {
		db.transaction = &Transaction{}
	}

	pipe := db.TxPipeline()

	db.transaction.trx = &pipe
	db.transaction.isUsed = true
}

func (db *DB) CommitTransaction(ctx context.Context) {
	if db.transaction == nil {
		return
	}

	pipe := *db.transaction.trx

	pipe.Exec(ctx)
	db.transaction = nil
}

func (db *DB) RollbackTransaction() {
	if db.transaction == nil {
		return
	}

	pipe := *db.transaction.trx

	pipe.Discard()
	db.transaction = nil
}

func (db *DB) IgnoreTransaction() {
	if db.transaction == nil {
		return
	}

	db.transaction.Ignore()
}

func (db *DB) GetDB(ctx context.Context) *redis.Pipeliner {
	if db.transaction == nil {
		pipe := db.Pipeline()
		defer pipe.Exec(ctx)

		return &pipe
	}

	if !db.transaction.isUsed {
		pipe := db.Pipeline()
		defer pipe.Exec(ctx)

		return &pipe
	}

	return db.transaction.trx
}
