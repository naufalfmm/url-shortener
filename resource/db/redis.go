package db

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/naufalfmm/url-shortener/resource/config"
)

func NewRedis(config *config.EnvConfig) (*DB, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddress,
		Password: config.RedisPassword,
		DB:       config.RedisDB,
	})

	return &DB{
		Client:      rdb,
		transaction: &Transaction{},
	}, nil
}
