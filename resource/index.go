package resource

import (
	"gitlab.com/naufalfmm/url-shortener/resource/config"
	"gitlab.com/naufalfmm/url-shortener/resource/db"
	"gitlab.com/naufalfmm/url-shortener/utils/validator"
)

type Resource struct {
	Config    *config.EnvConfig
	Redis     *db.DB
	Validator validator.Validator
}

func Init() (Resource, error) {
	conf, err := config.NewConfig()
	if err != nil {
		return Resource{}, err
	}

	rdb, err := db.NewRedis(conf)
	if err != nil {
		return Resource{}, err
	}

	validator, err := validator.NewV9()
	if err != nil {
		return Resource{}, err
	}

	return Resource{
		Config:    conf,
		Redis:     rdb,
		Validator: validator,
	}, nil
}
