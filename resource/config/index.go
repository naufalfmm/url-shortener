package config

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

type EnvConfig struct {
	ServiceName string `envconfig:"SERVICE_NAME" default:"Irrigation System API" required:"true"`
	ServicePort int    `envconfig:"SERVICE_PORT" default:"8080" required:"true"`

	// - Redis
	RedisAddress  string `envconfig:"REDIS_ADDRESS" required:"true"`
	RedisPassword string `envconfig:"REDIS_PASSWORD" required:"true"`
	RedisDB       int    `envconfig:"REDIS_NAME" default:"0" required:"true"`

	CurrentDomain string `envconfig:"CURRENT_DOMAIN" required:"true"`

	MaxGenerate int `envconfig:"MAX_GENERATE" default:"5" required:"true"`
}

func NewConfig() (*EnvConfig, error) {
	var config EnvConfig

	filename := os.Getenv("CONFIG_FILE")

	if filename == "" {
		filename = ".env"
	}

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		if err := envconfig.Process("", &config); err != nil {
			return nil, errors.Wrap(err, "failed to read from env variable")
		}
	}

	if err := godotenv.Load(filename); err != nil {
		return nil, errors.Wrap(err, "failed to read from .env file")
	}

	if err := envconfig.Process("", &config); err != nil {
		return nil, errors.Wrap(err, "failed to read from env variable")
	}

	return &config, nil
}
