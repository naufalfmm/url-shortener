package persistent

import "gitlab.com/naufalfmm/url-shortener/persistent/repository"

type Persistent struct {
	Repository repository.Repository
}

func Init(repository repository.Repository) (Persistent, error) {
	return Persistent{repository}, nil
}
