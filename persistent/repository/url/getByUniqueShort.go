package urlRepository

import (
	"context"
	"encoding/json"

	"github.com/go-redis/redis/v8"
	"gitlab.com/naufalfmm/url-shortener/consts"
	"gitlab.com/naufalfmm/url-shortener/model/dao"
)

func (r repository) GetByUniqueShort(ctx context.Context, uniqueShort string) (dao.Url, error) {
	strRes, err := r.resource.Redis.Get(ctx, generateKey(uniqueShort)).Result()
	if err == redis.Nil {
		return dao.Url{}, consts.ErrDataNotFound
	}

	if err != nil {
		return dao.Url{}, err
	}

	var urlData dao.Url
	if err := json.Unmarshal([]byte(strRes), &urlData); err != nil {
		return dao.Url{}, err
	}

	return urlData, nil
}
