package urlRepository

import (
	"context"
	"fmt"

	"gitlab.com/naufalfmm/url-shortener/model/dao"
	"gitlab.com/naufalfmm/url-shortener/resource"
)

type (
	Repository interface {
		GetByUniqueShort(ctx context.Context, uniqueShort string) (dao.Url, error)
		Create(ctx context.Context, url dao.Url) (dao.Url, error)
	}
	repository struct {
		resource resource.Resource
	}
)

func Init(resource resource.Resource) (Repository, error) {
	return &repository{
		resource: resource,
	}, nil
}

func generateKey(uniqueShort string) string {
	return fmt.Sprintf("unique_short_%s", uniqueShort)
}
