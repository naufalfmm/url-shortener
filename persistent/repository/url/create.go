package urlRepository

import (
	"context"

	"gitlab.com/naufalfmm/url-shortener/model/dao"
)

func (r repository) Create(ctx context.Context, url dao.Url) (dao.Url, error) {
	if err := r.resource.Redis.Set(ctx, generateKey(url.UniqueShort), url, 0).Err(); err != nil {
		return dao.Url{}, err
	}

	return url, nil
}
