package repository

import urlRepository "gitlab.com/naufalfmm/url-shortener/persistent/repository/url"

type Repository struct {
	Url urlRepository.Repository
}

func Init(url urlRepository.Repository) (Repository, error) {
	return Repository{
		Url: url,
	}, nil
}
