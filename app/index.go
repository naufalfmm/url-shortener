package app

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/naufalfmm/url-shortener/consts"
	"gitlab.com/naufalfmm/url-shortener/controller"
	urlController "gitlab.com/naufalfmm/url-shortener/controller/url"
	"gitlab.com/naufalfmm/url-shortener/middleware"
	"gitlab.com/naufalfmm/url-shortener/persistent"
	"gitlab.com/naufalfmm/url-shortener/persistent/repository"
	urlRepository "gitlab.com/naufalfmm/url-shortener/persistent/repository/url"
	"gitlab.com/naufalfmm/url-shortener/resource"
	"gitlab.com/naufalfmm/url-shortener/routes"
	"gitlab.com/naufalfmm/url-shortener/usecase"
	urlUsecase "gitlab.com/naufalfmm/url-shortener/usecase/url"
	"gitlab.com/naufalfmm/url-shortener/utils/generateResp"
)

type App struct {
	Ec         *echo.Echo
	Resource   resource.Resource
	Middleware middleware.Middleware
}

func Init() App {
	ec := echo.New()

	res, err := resource.Init()
	if err != nil {
		panic(err)
	}

	urlRepo, err := urlRepository.Init(res)
	if err != nil {
		panic(err)
	}

	repo, err := repository.Init(urlRepo)
	if err != nil {
		panic(err)
	}

	persist, err := persistent.Init(repo)
	if err != nil {
		panic(err)
	}

	urlUsec, err := urlUsecase.Init(persist, res)
	if err != nil {
		panic(err)
	}

	usec, err := usecase.Init(urlUsec)
	if err != nil {
		panic(err)
	}

	urlContr, err := urlController.Init(usec, res)
	if err != nil {
		panic(err)
	}

	contr, err := controller.Init(urlContr)
	if err != nil {
		panic(err)
	}

	midd, err := middleware.New(res)
	if err != nil {
		panic(err)
	}

	routes, err := routes.New(contr, midd)
	if err != nil {
		panic(err)
	}

	routes.Register(ec)

	return App{
		Ec:         ec,
		Resource:   res,
		Middleware: midd,
	}
}

func (app App) Run() {
	app.Ec.Use(app.Middleware.PanicRecover(), app.Middleware.ImplementCors())
	app.Ec.Validator = app.Resource.Validator

	app.Ec.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, generateResp.Default{
			Ok:      true,
			Message: fmt.Sprintf("Welcome to %s", app.Resource.Config.ServiceName),
		})
	})

	echo.NotFoundHandler = func(c echo.Context) error {
		return generateResp.NewJSONResponse(c, http.StatusNotFound, "", consts.ErrPathNotFound)
	}

	if err := app.Ec.Start(fmt.Sprintf(":%d", app.Resource.Config.ServicePort)); err != nil {
		panic(err)
	}
}
