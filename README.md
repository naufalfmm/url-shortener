# URL Shortener

## Getting started

URL Shortener is service for simplify the long url.

## How To Run

Please run `docker-compose -f docker-compose.yaml up --build`

## API Docs

### Simplify URL (POST /v1/url)

#### Request
{
    "longUrl": "https://ceritanya.url" (required)
}

#### Response (200)
{
    "ok": true,
    "message": "Success",
    "data": {
        "longUrl": "https://ceritanya.url",
        "uniqueShort": "yXUXAX",
        "shortUrl": "http://localhost:8080/yXUXAX",
        "redirectCount": 0,
        "createdAt": "2022-06-22T22:29:48.0620862Z"
    }
}

### Redirect URL (GET /v1/:uniqueShort)

#### Response (301)