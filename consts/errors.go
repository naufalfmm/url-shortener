package consts

import "errors"

var (
	ErrUnknownConstant              = errors.New("unknown constant")
	ErrConstantParsing              = errors.New("expected string for the constant")
	ErrInvalidTime                  = errors.New("invalid time")
	ErrInvalidToken                 = errors.New("invalid token")
	ErrUnclaimedToken               = errors.New("unclaimed token")
	ErrPathNotFound                 = errors.New("path not found")
	ErrDataNotFound                 = errors.New("data not found")
	ErrReachMaximumGenerateShortURL = errors.New("reach maximum generate short url")
)
