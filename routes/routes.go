package routes

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/naufalfmm/url-shortener/controller"
	"gitlab.com/naufalfmm/url-shortener/middleware"
)

type Routes struct {
	Controller controller.Controller
	Middleware middleware.Middleware
}

func New(controller controller.Controller, middleware middleware.Middleware) (Routes, error) {
	return Routes{
		Controller: controller,
		Middleware: middleware,
	}, nil
}

func (r *Routes) Register(ec *echo.Echo) {
	ec.Pre(r.Middleware.RemoveTrailingSlash())
	v1 := ec.Group("/v1")

	url := v1.Group("/url")
	url.POST("", r.Controller.Url.Create)

	ec.GET("/:uniqueShort", r.Controller.Url.Redirect)
}
