package middleware

import (
	"github.com/labstack/echo/v4"
	echMid "github.com/labstack/echo/v4/middleware"
)

func (m middleware) RemoveTrailingSlash() echo.MiddlewareFunc {
	return echMid.RemoveTrailingSlash()
}
