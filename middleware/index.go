package middleware

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/naufalfmm/url-shortener/resource"
)

type (
	Middleware interface {
		RemoveTrailingSlash() echo.MiddlewareFunc
		PanicRecover() echo.MiddlewareFunc
		ImplementCors() echo.MiddlewareFunc
	}

	middleware struct {
		Resource resource.Resource
	}
)

func New(resource resource.Resource) (Middleware, error) {
	return &middleware{resource}, nil
}
