package generateString

import "math/rand"

const stringList = "0123456789abcdefghijklmnopqrstuvwxxyzABCDEFGHIJKLMNOPQRSTUVWXXYZ"

func GenerateRandomString(length int) string {
	randStr := make([]byte, length)
	for i := 0; i < length; i++ {
		randStr[i] = stringList[rand.Intn(len(stringList))]
	}

	return string(randStr)
}
