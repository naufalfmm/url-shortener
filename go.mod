module gitlab.com/naufalfmm/url-shortener

go 1.16

require (
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-redis/redis/v8 v8.11.5 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.7.2 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pkg/errors v0.9.1
	go.opentelemetry.io/otel v0.16.0 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
