package main

import "gitlab.com/naufalfmm/url-shortener/app"

func main() {
	app.Init().Run()
}
