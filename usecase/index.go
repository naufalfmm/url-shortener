package usecase

import urlUsecase "gitlab.com/naufalfmm/url-shortener/usecase/url"

type Usecase struct {
	Url urlUsecase.Usecase
}

func Init(url urlUsecase.Usecase) (Usecase, error) {
	return Usecase{
		Url: url,
	}, nil
}
