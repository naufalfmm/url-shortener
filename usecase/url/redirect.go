package urlUsecase

import (
	"context"

	"gitlab.com/naufalfmm/url-shortener/model/dao"
	"gitlab.com/naufalfmm/url-shortener/model/dto"
)

func (u usecase) Redirect(ctx context.Context, req dto.RedirectURLRequest) (dao.Url, error) {
	urlData, err := u.persistent.Repository.Url.GetByUniqueShort(ctx, req.UniqueShort)
	if err != nil {
		return dao.Url{}, err
	}

	urlData.RedirectCount++

	return u.persistent.Repository.Url.Create(ctx, urlData)
}
