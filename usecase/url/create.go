package urlUsecase

import (
	"context"
	"errors"

	"gitlab.com/naufalfmm/url-shortener/consts"
	"gitlab.com/naufalfmm/url-shortener/model/dao"
	"gitlab.com/naufalfmm/url-shortener/model/dto"
)

func (u usecase) Create(ctx context.Context, req dto.CreateRequest) (dao.Url, error) {
	urlData := req.ToUrl()
	shortUnique, err := u.generateUniqueShortURL(ctx, urlData)
	if err != nil {
		return dao.Url{}, err
	}
	urlData.UniqueShort = shortUnique

	return u.persistent.Repository.Url.Create(ctx, urlData)
}

func (u usecase) generateUniqueShortURL(ctx context.Context, url dao.Url) (string, error) {
	for i := 0; i < u.resource.Config.MaxGenerate; i++ {
		shortUnique := url.GenerateUniqueShort()
		_, err := u.persistent.Repository.Url.GetByUniqueShort(ctx, shortUnique)
		if err != nil {
			if errors.Is(err, consts.ErrDataNotFound) {
				return shortUnique, nil
			}

			return "", err
		}
		continue
	}

	return "", consts.ErrReachMaximumGenerateShortURL
}
