package urlUsecase

import (
	"context"

	"gitlab.com/naufalfmm/url-shortener/model/dao"
	"gitlab.com/naufalfmm/url-shortener/model/dto"
	"gitlab.com/naufalfmm/url-shortener/persistent"
	"gitlab.com/naufalfmm/url-shortener/resource"
)

type (
	Usecase interface {
		Create(ctx context.Context, req dto.CreateRequest) (dao.Url, error)
		Redirect(ctx context.Context, req dto.RedirectURLRequest) (dao.Url, error)
	}
	usecase struct {
		persistent persistent.Persistent
		resource   resource.Resource
	}
)

func Init(persistent persistent.Persistent, resource resource.Resource) (Usecase, error) {
	return &usecase{
		persistent: persistent,
		resource:   resource,
	}, nil
}
