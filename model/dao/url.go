package dao

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/naufalfmm/url-shortener/resource/config"
	"gitlab.com/naufalfmm/url-shortener/utils/generateString"
)

type Url struct {
	LongURL       string    `json:"longUrl"`
	UniqueShort   string    `json:"uniqueShort"`
	RedirectCount uint64    `json:"redirectCount"`
	CreatedAt     time.Time `json:"createdAt"`
}

func (u Url) GenerateShortUrl(conf config.EnvConfig) string {
	return fmt.Sprintf("%s%s", conf.CurrentDomain, u.UniqueShort)
}

func (u Url) MarshalBinary() ([]byte, error) {
	return json.Marshal(u)
}

func (u Url) GenerateUniqueShort() string {
	return generateString.GenerateRandomString(6)
}
