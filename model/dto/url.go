package dto

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/naufalfmm/url-shortener/model/dao"
	"gitlab.com/naufalfmm/url-shortener/resource/config"
)

type (
	CreateRequest struct {
		LongURL string `json:"longUrl" validate:"required"`
	}

	RedirectURLRequest struct {
		UniqueShort string `param:"uniqueShort"`
	}

	UrlResponse struct {
		LongURL       string    `json:"longUrl"`
		UniqueShort   string    `json:"uniqueShort"`
		ShortURL      string    `json:"shortUrl"`
		RedirectCount uint64    `json:"redirectCount"`
		CreatedAt     time.Time `json:"createdAt"`
	}
)

func (req CreateRequest) ToUrl() dao.Url {
	return dao.Url{
		LongURL:       req.LongURL,
		RedirectCount: 0,
		CreatedAt:     time.Now(),
	}
}

func (req *CreateRequest) FromEchoContext(ec echo.Context) error {
	if err := ec.Bind(req); err != nil {
		return err
	}

	if err := ec.Validate(req); err != nil {
		return err
	}

	return nil
}

func (req *RedirectURLRequest) FromEchoContext(ec echo.Context) error {
	if err := ec.Bind(req); err != nil {
		return err
	}

	return nil
}

func NewUrlResponse(url dao.Url, conf config.EnvConfig) UrlResponse {
	return UrlResponse{
		LongURL:       url.LongURL,
		UniqueShort:   url.UniqueShort,
		ShortURL:      url.GenerateShortUrl(conf),
		RedirectCount: url.RedirectCount,
		CreatedAt:     url.CreatedAt,
	}
}
