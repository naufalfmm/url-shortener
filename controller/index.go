package controller

import urlController "gitlab.com/naufalfmm/url-shortener/controller/url"

type Controller struct {
	Url urlController.Controller
}

func Init(url urlController.Controller) (Controller, error) {
	return Controller{
		Url: url,
	}, nil
}
