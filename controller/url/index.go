package urlController

import (
	"gitlab.com/naufalfmm/url-shortener/resource"
	"gitlab.com/naufalfmm/url-shortener/usecase"
)

type Controller struct {
	Usecase  usecase.Usecase
	Resource resource.Resource
}

func Init(usecase usecase.Usecase, resource resource.Resource) (Controller, error) {
	return Controller{
		Usecase:  usecase,
		Resource: resource,
	}, nil
}
