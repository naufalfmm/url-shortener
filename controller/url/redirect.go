package urlController

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/naufalfmm/url-shortener/model/dto"
	"gitlab.com/naufalfmm/url-shortener/utils/generateResp"
)

func (c Controller) Redirect(ec echo.Context) error {
	var req dto.RedirectURLRequest
	if err := req.FromEchoContext(ec); err != nil {
		return generateResp.NewJSONResponse(ec, http.StatusBadRequest, err.Error(), err)
	}

	url, err := c.Usecase.Url.Redirect(ec.Request().Context(), req)
	if err != nil {
		return generateResp.NewJSONResponse(ec, http.StatusInternalServerError, err.Error(), err)
	}

	return ec.Redirect(http.StatusMovedPermanently, url.LongURL)
}
