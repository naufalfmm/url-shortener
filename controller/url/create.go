package urlController

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/naufalfmm/url-shortener/model/dto"
	"gitlab.com/naufalfmm/url-shortener/utils/generateResp"
)

func (c Controller) Create(ec echo.Context) error {
	var req dto.CreateRequest
	if err := req.FromEchoContext(ec); err != nil {
		return generateResp.NewJSONResponse(ec, http.StatusBadRequest, err.Error(), err)
	}

	url, err := c.Usecase.Url.Create(ec.Request().Context(), req)
	if err != nil {
		return generateResp.NewJSONResponse(ec, http.StatusInternalServerError, err.Error(), err)
	}

	return generateResp.NewJSONResponse(ec, http.StatusOK, "Success", dto.NewUrlResponse(url, *c.Resource.Config))
}
